﻿using UnityEngine;
using System.Collections;

public static class ParseExtensions{

	public static int TryParseToInt(this string toParse){
		int temp=0;
		if (!int.TryParse (toParse.Trim(), out temp)) {
			return 0;
		}
		return temp;
	}

	public static float TryParseToFloat(this string toParse){
		float result=0f;
		if (!float.TryParse (toParse.Trim(), out result)) {
			return 0;
		}
		return result;
	}
}

