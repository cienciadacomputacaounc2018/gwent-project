﻿using UnityEngine;
using System.Collections;

public enum CardsPosition
{
	Hand,
	Deck,
	Board,
	Graveyard,
	Banished
}

