﻿using UnityEngine;
using System.Collections;

public enum CardRank
{
	Bronze,
	Silver,
	Gold,
	Leader
}

