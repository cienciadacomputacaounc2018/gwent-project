﻿using UnityEngine;
using System.Collections;

public enum CardIDs
{
	Nekker,
	Forktail,
	Alzurs,
	GeraltIgni,
	Maerolorn,
	Mandrake,
	MarchingOrders,
	MonstersNest,
	NekkerWarrior,
	Ozzrel,
	Phoenix,
	RoyalDecree,
	Slyzard,
	VranWarrior,
	BrewessRitual,
	ArachasQueen

}

