﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

	public Deck deck;
	public Hand hand;
	public int remainingMulligans;
	public Text cardCount;
	public Text deckCount;
	public Text pointCounter;
	public Transform leaderposition;
}

