﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Card : MonoBehaviour {

	public CardIDs id;
	public Sprite sprite;
	public CardsPosition cardsPosition;
	public CardRank rank;
}
