﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionControl : MonoBehaviour {

	public Card currentCard;
	public Transform currentPlayer;
	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		if (currentCard != null) {
			this.transform.position = currentCard.transform.position;
		}
			
	}
}
