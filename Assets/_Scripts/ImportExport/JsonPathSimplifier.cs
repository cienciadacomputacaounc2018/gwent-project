﻿using UnityEngine;
using System.Collections;

public static class JsonPathSimplifier
{
	public static string currentFileName;

	public static string GetDirPath(string fileName, bool streamed){
		string dirPath ="";
		if (streamed) {
			dirPath = Application.streamingAssetsPath + "/Data/" + fileName;
		} else {
			dirPath = "Data/" + fileName;
		}
		return dirPath;
	}

}

