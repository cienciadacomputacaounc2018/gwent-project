﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public static class ParseToJson
{

	public static void UtilityParseTo<T>(T TData, string jsonPath){
		string json = JsonUtility.ToJson(TData);
		File.WriteAllText (jsonPath,json);
	}

	//
	public static T TryToParseFrom<T>(string path, bool streamedMode){
		if (streamedMode) {
			if (!File.Exists (path)) {
				Debug.Log ("Couldn't find " + path);
				return default(T);
			}
		}
		return UtilityParseFrom<T> (path, streamedMode);
	}

	public static T UtilityParseFrom<T>(string path, bool streamedMode){
		//	T TData = ScriptableObject.CreateInstance<T>();
		T TData;
		TData = Activator.CreateInstance<T>();

		string json="";
		if (streamedMode) {
			json = File.ReadAllText (path);
		}else {
			path = path.Replace (".json", "");
			Debug.Log (path);
			TextAsset textAsset;
			textAsset = Resources.Load (path) as TextAsset;
			if (textAsset)
				json = textAsset.text;
		}

		if (json==""){
			return default(T);
		}
		JsonUtility.FromJsonOverwrite(json, TData);
		return TData;
	}

}



