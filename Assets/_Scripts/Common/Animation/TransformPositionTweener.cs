﻿using UnityEngine;
using System.Collections;

public class TransformPositionTweener : Vector3Tweener 
{
	protected override void OnUpdate ()
	{
		base.OnUpdate ();
		transform.position = currentTweenValue;
	}
}

public class CharacterTransformPositionTweener : Vector3Tweener 
{
	protected override void OnUpdate ()
	{
		base.OnUpdate ();
		transform.position = new Vector3(currentTweenValue.x, currentTweenValue.y, transform.position.z);
	}
}
