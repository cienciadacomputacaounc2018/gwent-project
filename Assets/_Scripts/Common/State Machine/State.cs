using UnityEngine;
using System.Collections;

public abstract class State : MonoBehaviour 
{
	protected GameStateController owner;

	public virtual void Enter ()
	{
		owner = GetComponent<GameStateController> ();
		AddListeners();
	}
	
	public virtual void Exit ()
	{
		RemoveListeners();
	}

	protected virtual void OnDestroy ()
	{
		RemoveListeners();
	}

	protected virtual void AddListeners ()
	{

	}
	
	protected virtual void RemoveListeners ()
	{

	}
}