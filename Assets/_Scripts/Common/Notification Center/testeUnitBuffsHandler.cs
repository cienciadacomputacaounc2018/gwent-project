﻿using UnityEngine;
using System.Collections;

public class testeUnitBuffsHandler : MonoBehaviour {

	public const string buffsNotification = "eventodeteste";
	public int hp=100;
	int defesa=20;

	void Start () {
		StartCoroutine (Testezinho ());
	}
	
	IEnumerator Testezinho(){
		do {											//da pra passar absolutamente qualquer coisa(classes, métodos),
														//contanto que no args depois você recebe ele certo
			this.PostNotification (buffsNotification, defesa);
			yield return new WaitForSeconds (1f);
		} while (true);

	}
}
