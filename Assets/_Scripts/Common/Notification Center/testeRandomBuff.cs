﻿using UnityEngine;
using System.Collections;

public class testeRandomBuff : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (Testezinho ());
	}
	
	IEnumerator Testezinho(){
		yield return new WaitForSeconds (3f);
		this.AddObserver(metodo1, testeUnitBuffsHandler.buffsNotification);
		//this.AddObserver(metodo2, testeUnitBuffsHandler.buffsNotification);
		//this.AddObserver(metodo3, testeUnitBuffsHandler.buffsNotification);
	}
		//sender é o objeto que posta a notificação, args é o argumento que o sender manda junto
	void metodo1(object sender, object args){
		Debug.Log ("batata");
	}

	void metodo2(object sender, object args){
		testeUnitBuffsHandler handler = sender as testeUnitBuffsHandler;
		handler.hp -= 10;
		Debug.Log (handler.hp);
	}

	void metodo3(object sender, object args){
		int defesa = (int)args;
		testeUnitBuffsHandler handler = sender as testeUnitBuffsHandler;
		int dano = Random.Range (0, 100);
		dano = Mathf.Clamp (dano-defesa, 0, 100);
		handler.hp -= dano;
		Debug.Log ("hp "+handler.hp);
	}


}
