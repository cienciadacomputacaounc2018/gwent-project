﻿using UnityEngine;
using System.Collections;

public class MulliganState : State
{
	Player playerTurn;
	int currentSelected = 0;

	protected override void AddListeners ()
	{
		base.AddListeners ();
		playerTurn = owner.playerTurn;
		owner.selection.currentCard = playerTurn.hand.cards [0];
		this.AddObserver (UpButton, InputController.Up);
		this.AddObserver (DownButton, InputController.Down);
		this.AddObserver (RightButton, InputController.Right);
		this.AddObserver (LeftButton, InputController.Left);
		this.AddObserver (AcceptButton, InputController.Accept);
		this.AddObserver (BackButton, InputController.Back);
	}

	protected override void RemoveListeners ()
	{
		base.RemoveListeners ();
		this.RemoveObserver (UpButton, InputController.Up);
		this.RemoveObserver (DownButton, InputController.Down);
		this.RemoveObserver (RightButton, InputController.Right);
		this.RemoveObserver (LeftButton, InputController.Left);
		this.RemoveObserver (AcceptButton, InputController.Accept);
		this.RemoveObserver (BackButton, InputController.Back);
	}
		

	void UpButton(object sender, object args){
		
	}
	void DownButton(object sender, object args){

	}
	void RightButton(object sender, object args){
		//playerTurn.hand.cards[]
		currentSelected++;
		if (currentSelected == playerTurn.hand.cards.Count)
			currentSelected = 0;
		owner.selection.currentCard = playerTurn.hand.cards [currentSelected];
	}
	void LeftButton(object sender, object args){
		currentSelected--;
		if (currentSelected < 0)
			currentSelected = playerTurn.hand.cards.Count - 1;
		owner.selection.currentCard = playerTurn.hand.cards [currentSelected];
	}
	void AcceptButton(object sender, object args){
		//Chamar um Ienumerator 
		StartCoroutine(MulliganControl());
	}
	void BackButton(object sender, object args){

	}

	IEnumerator MulliganControl(){
		RemoveListeners ();
		yield return StartCoroutine (playerTurn.hand.Mulligan (
			playerTurn.hand.cards [currentSelected]));
		AddListeners ();
		owner.selection.currentCard = playerTurn.hand.cards [0];
		currentSelected = 0;
	}
}

