﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitGameState : State {


	public override void Enter ()
	{
		base.Enter ();
		Debug.Log ("iniciou");

		StartCoroutine (Init ());

	}

	IEnumerator Init(){
		yield return StartCoroutine (CreateCards (owner.player1));//player1

		yield return StartCoroutine (CardLeader (owner.player1));//player1



		owner.player1.deck.cards.Shuffle<Card>(new System.Random());



		yield return StartCoroutine(Draw(10, owner.player1));



		yield return StartCoroutine (CreateCards (owner.player2));//player2
		yield return StartCoroutine (CardLeader (owner.player2));//player1

		owner.player2.deck.cards.Shuffle<Card>(new System.Random());

		yield return StartCoroutine(Draw(10, owner.player2));

		yield return StartCoroutine (CoinFlip ());
		yield return new WaitForSeconds (1f);
		owner.ChangeState<MulliganState> ();
		//compra as cartas e
		//coloca as cartas na posição certa,
		//invoca e sorteia a moeda
		//proximoEstado
		}

	IEnumerator CreateCards(Player player){
		Deck deck = player.deck;
		for (int i = 0; i < deck.deckFechado.Count; i++) {
			Card card = CardFactory.CreateCard (deck.transform, owner.library,
				deck.deckFechado [i], FactionID.Monster);
			card.transform.name = CardsLibrary.cardNames [card.id];
			card.transform.localScale = new Vector3 (0.1f, 0.1f, 1);
			card.transform.parent = deck.transform;
			card.transform.localPosition = Vector3.zero;
			deck.cards.Add (card);
			yield return null;
		}
		player.deckCount.text = "" + deck.cards.Count;
	}

	IEnumerator Draw(int amount, Player player){
		Deck deck = player.deck;
		amount = 5;
		for (int i = 0; i < amount; i++) {
			if (deck.cards.Count == 0)
				break;
			Card card = deck.Draw ();
			yield return StartCoroutine (deck.hand.DrawAnimation (card));
			player.cardCount.text = "" + deck.hand.cards.Count;
			player.deckCount.text = ""+deck.cards.Count;
			yield return new WaitForSeconds(0.5f);
		}
	}

	IEnumerator CoinFlip(){

		int side = Random.Range (0, 2);
		owner.playerTurn = owner.player1;
		GameObject coin = Instantiate(Resources.Load<GameObject> ("Prefabs/coin")) as GameObject;
		coin.transform.position = owner.coinPosition.position;
		//Debug.Log (side); 0 é azul e 1 é vermelho
		side = side * 180;

		Tweener tweener = coin.transform.RotateToLocal (new Vector3 (0,  900+side, 0), 1.5f, EasingEquations.EaseOutCirc);
		while (tweener != null) {
			yield return null;
		}
	}

	IEnumerator CardLeader(Player player){
		Deck deck = player.deck;
		Card card = CardFactory.CreateCard (deck.transform, owner.library, CardIDs.ArachasQueen, FactionID.Monster);
		card.transform.name = CardsLibrary.cardNames [card.id];
		card.transform.localScale = new Vector3 (0.1f, 0.1f, 1);
		card.transform.position = player.leaderposition.position;
		yield return null;

		}

}
