﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
	public const string Up = "UpNotification";
	public const string Down = "DownNotification";
	public const string Left = "LeftNotification";
	public const string Right = "RightNotification";

	public const string Accept = "AcceptNotification";
	public const string Back = "BackNotification";
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetButtonDown("Up")){
			this.PostNotification(Up);
		}
		if(Input.GetButtonDown("Down")){
			this.PostNotification(Down);
		}
		if(Input.GetButtonDown("Right")){
			this.PostNotification(Right);
		}
		if(Input.GetButtonDown("Left")){
			this.PostNotification(Left);
		}
		if(Input.GetButtonDown("Accept")){
			this.PostNotification(Accept);
		}
		if(Input.GetButtonDown("Back")){
			this.PostNotification(Back);
		}
	}
}

