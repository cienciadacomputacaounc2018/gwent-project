﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour {

	public List<CardIDs> deckFechado;
	public Hand hand;
	public List<Card> cards;
	public bool teste;

	public List<CardIDs> blacklisted;

	void Start(){
		blacklisted = new List<CardIDs> ();
	}

	void Update(){
		if (teste) {
			teste = false;

		}
	}
	//method overloading
	public List<Card> Draw(int amount){
		List<Card> drawnCards = new List<Card> ();
		for (int i = 0; i < amount; i++) {
			Card card = Draw ();
			if(card!=null)
				drawnCards.Add (card);
		}
		return drawnCards;
	}

	public Card Draw(){
		Card draw = null;
		if (cards.Count > 0) {
			draw = cards [cards.Count - 1];
			cards.Remove (draw);
		}
		return draw;
	}

	public Card MulliganDraw(){
		Card draw = null;
		for (int i = cards.Count - 1; i >= 0; i--) {
			//Debug.Log ("Verificando a carta "+cards [i].id);
			if (blacklisted.Contains (cards [i].id)) { //verifica se esse ID está blacklistado, se está, pula pra próxima iteração
				continue;
			}else{
				draw = cards [i];
				cards.Remove (draw);
				break;
			}
		}
		if (cards.Count > 0 && draw == null) { //caso hajam cartas no deck, mas estejam todas blacklistadas
			draw = Draw ();
			//Debug.Log ("teve que comprar uma blacklistada mesmo");
		}
		return draw;
	}

	public void PlaceRandomly(Card card){
		cards.Insert (Random.Range (0, cards.Count), card);
	}

	public Card Mulligan(Card mulliganed){
		blacklisted.Add (mulliganed.id);
		PlaceRandomly (mulliganed);
		Card newCard = MulliganDraw ();
		return newCard;
	}


}
