﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hand : MonoBehaviour
{
	public Deck deck;
	public List<Card> cards;

	[Header("Mulligan teste")]
	public int cartaParaDescartar;
	public bool testeMulligan;

	Vector3 initialPos;
	Vector3 offset = new Vector3 (0.15f, 0, 0);

	void Awake(){
		initialPos = transform.position;
		cards = new List<Card> ();
	}

	void Update(){
		if (testeMulligan) {
			testeMulligan = false;
			StartCoroutine(Mulligan (cards [cartaParaDescartar]));
		}
	}

	public IEnumerator Mulligan(Card card){
		
		Card newCard = deck.Mulligan (card);
		yield return StartCoroutine (MulliganAnimation (card));
		yield return new WaitForSeconds (0.5f);
		yield return StartCoroutine (DrawAnimation (newCard));
	}

	public IEnumerator DrawAnimation(Card card){
		cards.Add (card);
		card.cardsPosition = CardsPosition.Hand;

		transform.position = initialPos- (offset*cards.Count);
		Vector3 position = initialPos+(offset*cards.Count);
		Tweener tweener = card.transform.CharacterMoveTo(position, 0.4f, 
			EasingEquations.Linear);


		while (tweener != null) {
			yield return null;
		}
		card.transform.parent = transform;

	}

	public IEnumerator MulliganAnimation(Card card){
		cards.Remove (card);
		card.cardsPosition = CardsPosition.Deck;

		transform.position = initialPos - (offset * cards.Count);
		Vector3 position = deck.transform.position;
		Tweener tweener = card.transform.CharacterMoveTo (position, 0.3f,
			                  EasingEquations.Linear);

		while (tweener != null) {
			yield return null;
		}
		for (int i = 0; i < cards.Count; i++) {
			cards [i].transform.localPosition = new Vector3(0,0,-0.62f)+((offset*2) * (i + 1));
		}

		card.transform.parent = deck.transform;
	}
}

