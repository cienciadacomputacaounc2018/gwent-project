﻿using UnityEngine;
using System.Collections;

public class GameStateController : StateMachine
{
	public CardsLibrary library;
	public Transform coinPosition;

	public Player player1;
	public Player player2;

	public SelectionControl selection;

	public Player playerTurn;
	public int roundMulligans = 3;

	void Start(){
		ChangeState<InitGameState> ();
	}

}