﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardsLibrary : MonoBehaviour
{

	public Dictionary<CardIDs, Sprite> loaded = new Dictionary<CardIDs, Sprite>();

	public static Dictionary<CardIDs, string> cardNames;

	void Awake(){
		cardNames = new Dictionary<CardIDs, string> ();
		cardNames.Add (CardIDs.Nekker, "nekker");
		cardNames.Add (CardIDs.Forktail, "forktail");
		cardNames.Add (CardIDs.Alzurs, "alzursDoubleCross");
		cardNames.Add (CardIDs.GeraltIgni, "geraltIgni");
		cardNames.Add (CardIDs.Maerolorn, "maerolorn");
		cardNames.Add (CardIDs.Mandrake, "mandrake");
		cardNames.Add (CardIDs.MarchingOrders, "marchingOrders");
		cardNames.Add (CardIDs.MonstersNest, "monsterNest");
		cardNames.Add (CardIDs.NekkerWarrior, "nekkerWarrior");
		cardNames.Add (CardIDs.Ozzrel, "ozzrel");
		cardNames.Add (CardIDs.Phoenix, "phoenix");
		cardNames.Add (CardIDs.RoyalDecree, "royalDecree");
		cardNames.Add (CardIDs.Slyzard, "slyzard");
		cardNames.Add (CardIDs.VranWarrior, "vranWarrior");
		cardNames.Add (CardIDs.BrewessRitual, "brewessRitual");
		cardNames.Add (CardIDs.ArachasQueen, "arachasQueen");
	}
}