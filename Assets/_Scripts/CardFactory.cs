﻿using UnityEngine;
using System.Collections;

public class CardFactory: MonoBehaviour
{

	public static Card CreateCard(Transform parent, CardsLibrary library, CardIDs cardId, FactionID factionID){
		Card card = null;
		GameObject newGO;
		Unit unit;
		Spell spell;
		Sprite art;
		newGO =  Instantiate(Resources.Load<GameObject> ("Prefabs/cardStandard")) as GameObject;

		if (library.loaded.ContainsKey (cardId)) { //with this scheme each sprite is loaded from assets only once
			art = library.loaded [cardId];
		} else {
			art = Resources.Load<Sprite> ("Cards/"+CardsLibrary.cardNames[cardId]);
			library.loaded.Add (cardId, art);
		}
		newGO.GetComponent<SpriteRenderer> ().sprite = art;

		switch (cardId) {
		case CardIDs.Nekker:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 4;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Bronze;
	
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.ArachasQueen:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 7;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Leader;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.Forktail:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 7;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Bronze;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.BrewessRitual:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 1;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Gold;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.GeraltIgni:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 5;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Gold;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.Maerolorn:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 4;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Silver;
			FactionBackArt (newGO.transform, FactionID.Monster);
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.Mandrake:
			spell = newGO.AddComponent<Spell> ();
			spell.rank = CardRank.Silver;
			break;
		case CardIDs.MarchingOrders:
			spell = newGO.AddComponent<Spell> ();
			spell.rank = CardRank.Silver;
			break;
		case CardIDs.MonstersNest:
			spell = newGO.AddComponent<Spell> ();
			spell.rank = CardRank.Silver;
			break;
		case CardIDs.NekkerWarrior:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 9;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Bronze;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.Ozzrel:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 5;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Silver;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.Phoenix:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 5;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Gold;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.RoyalDecree:
			spell = newGO.AddComponent<Spell> ();
			spell.rank = CardRank.Gold;
			break;
		case CardIDs.Alzurs:
			spell = newGO.AddComponent<Spell> ();
			spell.rank = CardRank.Silver;
			break;
		case CardIDs.Slyzard:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 2;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Bronze;
			AddPowerCorner (newGO.transform);
			break;
		case CardIDs.VranWarrior:
			unit = newGO.AddComponent<Unit> ();
			unit.powerBase = 6;
			unit.powerCurrent = unit.powerBase;
			unit.rank = CardRank.Bronze;
			AddPowerCorner (newGO.transform);
			break;
		default:
			Debug.Log ("ID not found");
			break;
		}
		FactionBackArt (newGO.transform, factionID);
		card = newGO.GetComponent<Card> ();
		card.id = cardId;
		card.cardsPosition = CardsPosition.Deck;

		return card;
	}

	public static void FactionBackArt(Transform cardTransform, FactionID id){
		GameObject newGO = null;;

		switch (id) {
		case FactionID.Scoiatel:
			break;

		default:
			newGO =  Instantiate(Resources.Load<GameObject> ("FactionsImages/faction-monsters")) as GameObject;
			break;
		}
		newGO.transform.parent = cardTransform;
		newGO.transform.localPosition = new Vector3 (0, 0, 0.005f);
	}

	public static void AddPowerCorner(Transform cardTransform){
		GameObject newGO =  Instantiate(Resources.Load<GameObject> ("Prefabs/powerCorner"));
		newGO.transform.parent = cardTransform;
		newGO.transform.localPosition = new Vector3 (-0.88f, 1.2f, -0.005f);
	}
}

